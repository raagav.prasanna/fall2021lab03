// Raagav Prasanna 2036159
package LinearAlgebra;
public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return(this.x);
    }

    public double getY() {
        return(this.y);
    }

    public double getZ() {
        return(this.z);
    }

    public double magnitude() {
        double magnitudeVal = Math.sqrt( (Math.pow(this.x,2)) + (Math.pow(this.y,2)) + (Math.pow(this.z,2)) );
        return(magnitudeVal);
    }

    public double dotProduct(Vector3d inpVec) {
        double dotProductVal = ( this.x * inpVec.getX() )  + ( this.y * inpVec.getY() ) + ( this.z * inpVec.getZ() );
        return(dotProductVal);
    }

    public Vector3d add(Vector3d inpVec) {
        Vector3d retVec = new Vector3d(( this.x + inpVec.getX() ), ( this.y + inpVec.getY() ), ( this.z + inpVec.getZ() ));
        return(retVec);
    }
}