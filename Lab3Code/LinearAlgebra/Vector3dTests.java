// Raagav Prasanna 2036159
package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class Vector3dTests {
    @Test
    public void testGetMethods() {
        Vector3d v1 = new Vector3d(5, 6, 7);
        double x = v1.getX();
        double y = v1.getY();
        double z = v1.getZ();

        assertEquals(5, x);
        assertEquals(6, y);
        assertEquals(7, z);
    }

    @Test
    public void testMagnitude() {
        Vector3d v1 = new Vector3d(3, 4, 0);
        double magVal = v1.magnitude();

        assertEquals(5, magVal);
    }

    @Test
    public void testDotProduct() {
        Vector3d v1 = new Vector3d(1, 1, 2);
        Vector3d v2 = new Vector3d(2, 2, 3);

        double dotProdval = v1.dotProduct(v2);

        assertEquals(10, dotProdval);
    }

    @Test
    public void testAddFunc() {
        Vector3d v1 = new Vector3d(3, 4, 0);
        Vector3d v2 = new Vector3d(2, 2, 3);

        Vector3d v3 = v1.add(v2);

        assertEquals(5, v3.getX());
        assertEquals(6, v3.getY());
        assertEquals(3, v3.getZ());
    }
}