package LinearAlgebra;
public class VectorTest {
    public static void main(String[] args) {
        Vector3d vec1 = new Vector3d(5, 6, 7);

        Vector3d vec2 = new Vector3d(2, 2, 3);


        //System.out.println(vec1.magnitude());

        //System.out.println(vec1.dotProduct(vec2));

        Vector3d vec3 = vec1.add(vec2);

        System.out.println(vec3.magnitude());

    }
}